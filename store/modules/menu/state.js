const state = () => ({
    globalMenu: [
        {
            parentName: '메인 보드',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'DASH_BOARD', currentName: '대시 보드', link: '/', isShow: true },
                ]
        },
        {
            parentName: '회원 관리',
            icon: 'el-icon-user',
            menuLabel: [
                { id: 'MEMBER_SEARCH', currentName: '회원 정보 조회', link: '/my-menu/member/member-search', isShow: true },
                { id: 'MEMBER_CHANGE', currentName: '회원 정보 변경', link: '/my-menu/member/member-change', isShow: true },
            ]
        },
        {
            parentName: '콘텐츠 관리',
            icon: 'el-icon-postcard',
            menuLabel: [
                { id: 'CONTENT_SEARCH', currentName: '게시글 조회', link: '/my-menu/content/content-topic-search', isShow: true },
                { id: 'COMMENT_SEARCH', currentName: '댓글 조회', link: '/my-menu/content/content-comments-search', isShow: true },
            ]
        },
        {
            parentName: '금칙어 관리',
            icon: 'el-icon-circle-close',
            menuLabel: [
                { id: 'FORBID_SEARCH', currentName: '금칙어 조회', link: '/my-menu/forbid/forbid-search', isShow: true },
                { id: 'FORBID_REGIST', currentName: '금칙어 등록', link: '/my-menu/forbid/forbid-regist', isShow: true },
            ]
        },
        {
            parentName: '1:1 상담',
            icon: 'el-icon-edit',
            menuLabel: [
                { id: 'COUNSEL_SEARCH', currentName: '1:1 상담 내역 조회', link: '/my-menu/ask/ask-search', isShow: true },
                { id: 'COUNSEL_REGIST', currentName: '1:1 상담 내역 등록', link: '/my-menu/ask/ask-regist', isShow: true },
            ]
        },
        {
            parentName: '통계',
            icon: 'el-icon-data-line',
            menuLabel: [
                { id: 'STATS_CONTENT', currentName: '기간별 접속자 수 조회', link: '/my-menu/stats/stats-content', isShow: true },
                { id: 'STATS_MEMBER', currentName: '회원 현황 조회', link: '/my-menu/stats/stats-member', isShow: true },
            ]
        },
        {
            parentName: '홈페이지 설정',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'WEB_SETTING', currentName: '사이트 설정', link: '/my-menu/setting/setting-web', isShow: true },
                { id: 'APP_SETTING', currentName: '앱 설정', link: '/my-menu/setting/setting-app', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
