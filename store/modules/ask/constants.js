export default {
    DO_ASK_LIST: 'ask/doAskList',
    DO_ASK_LIST_PAGING: 'ask/doAskListPaging',
    DO_ASK_DETAIL: 'ask/doAskDetail',
    DO_ASK_CREATE: 'ask/doAskCreate',
    DO_ASK_DELETE: 'ask/doAskDelete',
}

/* 행위에 대한 이름 짓기, api-urls에서 행위를 가져옴 */
