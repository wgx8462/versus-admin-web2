const BASE_URL = '/v1/member-ask'
const BASE_URL1 = '/v1/ask-management'

export default {
    DO_ASK_LIST: `${BASE_URL}/all`, //get
    DO_ASK_LIST_PAGING: `${BASE_URL}/all/{pageNum}`, //get
    DO_ASK_DETAIL: `${BASE_URL}/detail/memberAsk-id/{memberAskId}`, //get
    DO_ASK_CREATE: `${BASE_URL1}/join/member-ask-id/{memberAskId}`, //post
    DO_ASK_DELETE: `${BASE_URL}/delete/{id}`, //del
}

/* 기본 주소 설정, 바뀌는 주소 설정하기 */
