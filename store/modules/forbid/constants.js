export default {
    DO_FORBID_LIST: 'forbid/doForbidList',
    DO_FORBID_PAGE_LIST: 'forbid/doForbidPageList',
    DO_FORBID_DELETE: 'forbid/doForbidDelete',
    DO_FORBID_CREATE: 'forbid/doForbidCreate'
}

/* 행위에 대한 이름 짓기, api-urls에서 행위를 가져옴 */
