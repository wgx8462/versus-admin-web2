const BASE_URL = '/v1/forbid'

export default {
    DO_FORBID_LIST: `${BASE_URL}/all`, //get
    DO_FORBID_PAGE_LIST: `${BASE_URL}/{pageNum}/all`, //get
    DO_FORBID_DELETE: `${BASE_URL}/delete/forbid-id/{forbidId}`, //del
    DO_FORBID_CREATE: `${BASE_URL}/new`, //post
}

/* 기본 주소 설정, 바뀌는 주소 설정하기 */
