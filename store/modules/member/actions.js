import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.ALL]: (store) => {
        return axios.get(apiUrls.ALL)
    },
}
