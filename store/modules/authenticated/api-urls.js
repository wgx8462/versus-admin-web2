const BASE_URL = '/v1/login'

export default {
    DO_LOGIN: `${BASE_URL}/member`, //post
    DO_PASSWORD_CHANGE: `${BASE_URL}/profile/password`, //put
    DO_PROFILE: `${BASE_URL}/profile/info`, //get
}
